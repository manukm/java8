package lambda;

import org.junit.Test;

import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Created by Manu on 16-Nov-14.
 */
public class ReturnFunction {

    Function<Integer, Predicate<Integer>> isGreaterThan = pivot -> number -> number > pivot;

    @Test
    public void print_all_numbers_greater_than_2() {
        Stream<Integer> ints = Stream.of(1, 2, 3, 5, 4, 6, 7, 9, 8);

        System.out.println("Numbers greater than 2");
        ints.filter(isGreaterThan.apply(2)).forEach(num -> System.out.print(num + " "));
    }

    @Test
    public void print_all_numbers_greater_than_6() {
        Stream<Integer> ints = Stream.of(1, 2, 3, 5, 4, 6, 7, 9, 8);

        System.out.println("Numbers greater than 6");
        ints.filter(isGreaterThan.apply(6)).forEach(num -> System.out.print(num + " "));
    }
}
