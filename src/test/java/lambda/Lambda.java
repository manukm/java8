package lambda;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Manu on 08-Nov-14.
 */
public class Lambda {

    @Test
    public void non_lambda() {
        List<String> names = Arrays.asList("Gosling", "Joy", "Bloch", "Khosla", "Goetz");

        Collections.sort(names, (name1, name2) -> Integer.compare(name1.length(), name2.length()));
        System.out.println("Sorted names = " + names);

        Stream<String> sorted = names.stream().sorted((name1, name2) -> Integer.compare(name1.length(), name2.length()));
        System.out.println(sorted.collect(Collectors.toList()));
    }

    @Test
    public void lambda_thread() {
        Runnable r = () -> System.out.println(Thread.currentThread().getName());
        Thread t = new Thread(r, "With lambda !!");
        t.start();
    }

    @Test
    public void lambda_thread_2() {
        Thread t = new Thread(() -> System.out.println("Hello lambda.Lambda !!"));
        t.start();
    }
}
