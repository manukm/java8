package stream;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by Manu on 16-Nov-14.
 */
public class Mapping {

    @Test
    public void filter_mapping_sorting() {
        List<Integer> myList = Arrays.asList(8, 1, 7, 4, 6, 3, 9, 5, 2, 12);

        myList.stream()
                .filter(num -> num % 2 == 0)  // intermediate operations
                .map(num -> num * num)
                .sorted()
                .forEach(num -> System.out.print(num + " "));    // terminal opearation
    }

    @Test
    public void iterate_with_limit() {
        Stream.iterate(1, number -> number + 1)
                .map(number -> number * number)
                .limit(5)
                .forEach(number -> System.out.print(number + "    "));
    }
}
