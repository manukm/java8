package stream;

import org.junit.Test;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;
import java.util.stream.Stream;
;

/**
 * Created by Manu on 16-Nov-14.
 */
public class Reducer {

    @Test
    public void count() {
        Stream<Integer> numbers = Stream.of(3, 6, 4, 5, 8, 7, 1);

        Optional<Integer> sum = numbers.reduce((x, y) -> x + y);
        // Optional<Integer> sum = numbers.reduce( Integer::sum );
        System.out.println("Sum = " + sum.orElse(0));  //

        long count1 = numbers.count();
        Optional<Integer> min = numbers.min(Comparator.naturalOrder());  // Provide comparing function
        Optional<Integer> man = numbers.max(Comparator.naturalOrder());  // Provide comparing function
    }


    @Test
    public void parallel_stream() {

        List<Integer> numbers = Arrays.asList(3, 6, 4, 5, 8, 7, 1, 10, 20);

        Optional<Integer> sum = numbers.parallelStream().reduce((x, y) -> x + y);

        System.out.println("Sum = " + sum.orElse(0));  //

        long count1 = numbers.stream().count();
        Optional<Integer> min = numbers.stream().min(Comparator.naturalOrder());  // Provide comparing function
        Optional<Integer> man = numbers.stream().max(Comparator.naturalOrder());  // Provide comparing function
    }
}
