package stream;

import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Grouping {

    @Test
    public void grouping() {
        Stream<String> countryStream = Stream.of("India", "Pak", "Bhutan", "Nepal");
        Map<Integer, List<String>> lengthToNameMap = countryStream.collect(
                Collectors.groupingBy(String::length));
        System.out.println(lengthToNameMap);
    }

    @Test
    public void grouping_and_count() {
        Stream<String> countryStream = Stream.of("India", "Pak", "Bhutan", "Nepal");
        Map<Integer, Long> lengthToCount = countryStream.collect(
                Collectors.groupingBy(String::length, Collectors.counting()));
        System.out.println(lengthToCount);
    }

    @Test
    public void partitioning() {
        Stream<String> countryStream = Stream.of("India", "Pak", "Bhutan", "Nepal");
        Map<Boolean, List<String>> lengthToNameMap = countryStream.collect(
                Collectors.partitioningBy((country) -> country.length() == 5));
        System.out.println(lengthToNameMap);
    }
}
