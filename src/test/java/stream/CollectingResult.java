package stream;

import org.junit.Test;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class CollectingResult {
    @Test
    public void collect_to_array() {
        Stream<String> countryStream = Stream.of("India", "Pak", "Bhutan", "Nepal");
        //Object[] countriesCollected = countryStream.toArray();
        String[] countries = countryStream.toArray(String[]::new);
        System.out.println(Arrays.asList(countries));
    }

    @Test
    public void collect_to_list() {
        Stream<String> countryStream = Stream.of("India", "Pak", "Bhutan", "Nepal");
        List<String> countriesList = countryStream.collect(Collectors.toList());
        System.out.println(countriesList);
    }

    @Test
    public void collect_to_map() {
        Stream<String> countryStream = Stream.of("India", "Pak", "Bhutan");
        Map<Integer, String> lengthToNameMap = countryStream.collect(
                Collectors.toMap(
                        country -> country.length(),
                        country -> country
                )
        );
        System.out.println(lengthToNameMap);
    }

    @Test
    public void to_map_replace_existing() {
        Stream<String> countryStream = Stream.of("India", "Pak", "Bhutan", "Nepal");
        Map<Integer, String> lengthToNameMap = countryStream.collect(
                Collectors.toMap(
                        String::length,
                        Function.identity(),
                        (existing, newValue) -> newValue));
        System.out.println(lengthToNameMap);
    }

    @Test
    public void to_map_keep_all() {
        Stream<String> countryStream = Stream.of("India", "Pak", "Bhutan", "Nepal");
        Map<Integer, List<String>> lengthToNameMap = countryStream.collect(
                Collectors.toMap(
                        String::length,
                        (country) -> Collections.singletonList(country),
                        (existingNames, newNames) -> {
                            List<String> newNamesList = new ArrayList<>(newNames);
                            newNamesList.addAll(existingNames);
                            return newNamesList;
                        },
                        TreeMap::new
                ));
        System.out.println(lengthToNameMap);
    }

    @Test
    public void even_square() {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);
        List<Integer> evens = numbers.stream()
                .filter(num -> num % 2 == 0)
                .map(num -> num * num)
                .collect(toList());

        System.out.println(evens);
    }
}
