import org.junit.Test;

public class DefaultMethod {
    @Test
    public void default_method_test() {
        X x = new X();
        x.method1();
    }
}

interface Parent1 {
    default void method1() {
        System.out.println("Default method-1 from Parent1");
    }
}

interface Child extends Parent1 {
    default void method1() {
        System.out.println("Default method-1 from Child");
    }
}

interface Parent2 {
    default void method1() {
        System.out.println("Default method-1 from Parent2");
    }
}

class X implements Parent2, Child {
    @Override
    public void method1() {
        Child.super.method1();
    }

//    @Override
//    public void method1() {
//        System.out.println("method-1 from X");
//    }
}




