import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by Manu on 15-Nov-14.
 */
public class MethodReferenceTest {
    @Test
    public void with_method_reference() {
        List<String> names = Arrays.asList("Gosling", "Joy", "Bloch", "Khosla", "Goetz");

        Collections.sort(names, MethodReferenceTest::compareNames);
        System.out.println("Sorted names = " + names);

        String shortestName = names.stream().min(MethodReferenceTest::compareNames).get();
        System.out.println("ShortestName = " + shortestName);
    }

    private static int compareNames(String name1, String name2) {
        return name1.length() - name2.length();
    }


    @Test
    // Code duplication
    public void non_method_reference() {
        List<String> names = Arrays.asList("Gosling", "Joy", "Bloch", "Khosla", "Goetz");

        Collections.sort(names, (name1, name2) -> name1.length() - name2.length());
        System.out.println("Sorted names = " + names);

        String shortestName = names.stream().min((name1, name2) -> name1.length() - name2.length()).get();
        System.out.println("ShortestName" + shortestName);
    }
}
